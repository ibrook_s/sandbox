export const createRequest = function () {
    return {
        type: 'ACTION_CREATE_REQUEST',
        payload: {
            loading: true
        }
    }
};
export const finishRequest = function (data) {
    return {
        type: 'ACTION_CREATE_REQUEST_FINISH',
        payload: {
            loading: false,
            data
        }
    }
};

export function request(url) {
    return (dispatch) => {
        dispatch(createRequest());
        return fetch(url)
            .then((data) => data.json())
            .then((response) => {
                console.log(response);
                dispatch(finishRequest(response));
            })
            .catch((e) => {
                if (e) alert('Error');
            })
    }
}