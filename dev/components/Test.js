import React, {Component} from 'react';

export class Test extends Component {
    constructor() {
        super();
        this.state = {
            currentPosition: null,
            currentPositionWatcher: null
        }
    }
    positionSuccess = (position) => {
        console.log('Current position is ', position.coords);
        this.setState({
            currentPosition: position.coords
        })
    };
    positionError = (err) => {
        console.log('Something not work');
    };
    startPosWatcher = () => {
        this.setState({
            currentPositionWatcher: navigator.geolocation.watchPosition(this.positionSuccess, this.positionError)
        });
    };
    stopPosWatcher = () => {
        navigator.geolocation.clearWatch(this.state.currentPositionWatcher)
    };
    componentWillMount() {
        if ("geolocation" in navigator) {
            this.startPosWatcher();
        } else {
            console.log('Navigation nor work');
        }
    }
    render() {
        return (<div>
            {
                !this.state.currentPosition
                    ? <h3>Определение местоположения....</h3>
                    : <div>Текущее местоположение
                        {this.state.currentPosition.latitude} - долгота и
                        {this.state.currentPosition.longitude} - широта
                    </div>
            }
        </div>)
    }
}