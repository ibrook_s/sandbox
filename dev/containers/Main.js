import React, { Component } from 'react';
import { Header } from '../components/Header.js';
import { connect } from "react-redux";
import { request } from "../actions/Test";
import { Test } from '../components/Test.js';
class MainContainer extends Component {
    render() {
        const users = this.props.test.data;
        return (
            <div>
                <Header/>
                <h1>
                    {
                        !this.props.test.loading
                            ? users.map((user) => {
                                    return <li>{user.name}</li>
                                })
                            : <h3>Loading..</h3>
                    }
                    <input type="button" value="Test loading" onClick={
                        () => this.props.request('http://www.mocky.io/v2/5abb8f5d2d000053009bdd8d')
                    } />
                </h1>

                <Test></Test>
            </div>)
    }
}

function mapDispatchToProps (dispatch) {
    return {
        request: (url) => dispatch(request(url))
    }
}
function mapStateToProps (state) {
    return {
        test: state.test
    }
}
export const Main = connect(mapStateToProps, mapDispatchToProps)(MainContainer);