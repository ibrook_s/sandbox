const initialState = {
    get() {
        return {
            loading: true
        }
    }
};
export const testReducer = function (state = initialState.get(), action) {
    console.log('Текущий стейт: ', state);
    switch (action.type) {
        case 'ACTION_CREATE_REQUEST':
            return {
                ...state,
                loading: true,
            };
        case 'ACTION_CREATE_REQUEST_FINISH':
            return {
                ...state,
                loading: false,
                data: action.payload.data
            };
        default:
            return state;
    }
};