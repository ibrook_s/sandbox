import { combineReducers } from 'redux';
import { testReducer } from './Test.js'

export default combineReducers(
    {
        test: testReducer
    }
)