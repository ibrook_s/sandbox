import {applyMiddleware, createStore} from 'redux';
import reducer from '../reducers/reducer.js';
import thunk from '../middlewares/thunk.js';

export const store = createStore(reducer, applyMiddleware(thunk));