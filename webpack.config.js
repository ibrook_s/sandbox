const webpack = require('webpack');
const path = require('path');

module.exports = {
    entry: {
        main: __dirname + "/dev/main.jsx"
    },
    output: {
        filename: "[name].js",
        path: __dirname + '/public/js'
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader",
                    options: {
                        presets: ["es2015", "stage-0", "react"]
                    }
                }
            }
        ]
    },
    devServer: {
        contentBase: path.resolve(__dirname, "public"),
        port: 1337,
        open: true,
        hot: false,
    }
};